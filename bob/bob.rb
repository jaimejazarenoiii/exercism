class Bob

  def self.hey(remark)
    
    if remark.strip.empty?
      "Fine. Be that way!"
    elsif remark.end_with?("?") && (remark != remark.upcase || remark == remark.downcase)
      "Sure."
    elsif remark == remark.upcase && remark.downcase != remark
      "Whoa, chill out!"
    else
      "Whatever."
    end
        
  end

end