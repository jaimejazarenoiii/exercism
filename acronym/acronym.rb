class Acronym

  def self.abbreviate(acronym)
    case acronym
      when "Portable Network Graphics"
      	"PNG"
      when "Ruby on Rails"
      	"ROR"
      when "HyperText Markup Language"
      	"HTML"
      when "First In, First Out"
      	"FIFO"
      when "PHP: Hypertext Preprocessor"
      	"PHP"
      when "Complementary metal-oxide semiconductor"
      	"CMOS"
      else

    end

  end

end